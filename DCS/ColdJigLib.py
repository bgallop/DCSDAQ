from __future__ import print_function
import time
import graphyte
import threading
import thread
from zmq_client import Client
import re
import sht_sensor
try:
    from queue import queue
except ImportError:
    from Queue import Queue as queue
import DAQListner
from DCSTools import message_parser

from collections import deque
import numpy as np
def check_stability(temperature_deque):
    tolerance=0.2
    return np.std(temperature_deque)<tolerance

#local configuration
from initialize import SensorDict
from initialize import master,grafana
from initialize import dcs_dict
print("dcs_dict")
print(dcs_dict)
from initialize import IVCurveConfig
import HVTools
DAQ_client=Client("10.2.241.143","5555")
graphyte.init(grafana)
CommQueue= queue()
CycleQueue=queue()

def Ping():
    return DAQ_client.Ping()
def start_sensors():
    sensor_thread=SensorThread("sensor thread",CommQueue,LV=dcs_dict['LV'],HV=dcs_dict['HV'])
    sensor_thread.start()

def stop_sensors():
    CommQueue.put("break")
                
def do_IVcurve():
    #run IV curve test in seperate thread
    step,end,start=IVCurveConfig()
    thread.start_new_thread(HVTools.IVCurve, (self.HV,step,end,start,))
 
def LV_TurnOn():
    try:
        dcs_dict['LV'].TurnOn()
        return True
    except:
        return False

def LV_TurnOff():
    try:
        dcs_dict['LV'].TurnOff()
        return True
    except:
        return False

def LV_SetVoltage(value,channel):
    try:
        dcs_dict['LV'].SetVoltage(value,channel)
        return True
    except:
        return False
    
def LV_SetCurrent(value,channel):
    try:
        dcs_dict['LV'].SetCurrent(value,channel)
        return True
    except:
        return False
    
def LV_SetVoltageProtection(value,channel):
    try:
        dcs_dict['LV'].SetVoltageProtection(value,channel)
        return True
    except:
        return False
    

def LV_GetCurrent(channel):
    try:
        current=dcs_dict['LV'].GetActualCurrent(channel)
        return current
    except:
        return None
    
def LV_GetVoltage(channel):
    try:
        voltage=dcs_dict['LV'].GetVoltage(channel)
        return voltage
    except:
        return None
    
def HV_TurnOn():
    try:
        dcs_dict['HV'].TurnOn()
        return True
    except:
        return None
def HV_TurnOff():
    try:
        dcs_dict['HV'].TurnOff()
        return True
    except:
        return None
    
def HV_SetCompliance(value):
    try:
        dcs_dict['HV'].SetCurrentCompliance(value)
        return True
    except:
        return None

def HV_SetRange(voltage_range):
    try:
        dcs_dict['HV'].SetVoltageRange(value)
        return True
    except:
        return None
    
def HV_SetVoltage(value):
    try:
        dcs_dict['HV'].SetVoltageLevel(value)
        return True
    except:
        return None
    
def HV_SetCurrent(value):
    try:
        dcs_dict['HV'].SetCurrentLevel(value)
        return True
    except:
        return None
    
def HV_GetVoltage():
    try:
        current=dcs_dict['HV'].GetVoltage()
        return current
    except:
        return None
def HV_GetCurrent():
    try:
        current=dcs_dict['HV'].GetCurrent()
        return current
    except:
        return None
    
def Chiller_SetTemperature(value):
    try:    
        dcs_dict['Chiller'].SetTemperature(value)
        return True
    except TypeError:
        return False

    
def Chiller_TurnOn():
    try:
        dcs_dict['Chiller'].TurnOn()
        return True
    except:
        return False
def Chiller_TurnOff():
    try:
        dcs_dict['Chiller'].TurnOff()
        return True
    except:
        return False
def Chiller_GetTemperature():
    temp=dcs_dict['Chiller'].GetTemperature()
    return temp
#return None

def do_trims(msg):
    msg_data=msg.split()
    msg_data[3]="true"
    msg_data[4]="true"
    return " ".join(msg_data)


def Start_ThermalCycle(message,lower_temp,upper_temp):
    global DAQ_client
    global CycleQueue
    DAQListner.start()
    #message is the tests to run
    sensor=SensorDict["ThermoCouple_logger"]
    print("coldjiglib start thermal cycle")
    cycler=ThermalCycle("Thermal Cycle",CycleQueue,DAQ_client,sensor,dcs_dict['Chiller'],message,target_temperature=int(lower_temp),room_temperature=int(upper_temp))
    cycler.start()
    
def Thermal_Cycle(command):
    global CycleQueue
    print("changed cycle")
    CycleQueue.put(command)
    
class SensorThread(threading.Thread):
    def __init__(self,threadID,comm_queue,interlock=None,LV=None,HV=None):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.queue=comm_queue
        self.lock = threading.Lock()
        self.interlock=interlock
        global master
        global grafana
        self.LV=LV
        self.HV=HV
        print("Granafa Server:",grafana)
        
        self.MasterServer=Client(master,"5554")
    

    def run(self):
        print ("Starting " + str(self.threadID))
        self.Read()

    def Read(self):
        command=""
        while True:
            if (not self.queue.empty()):
                command=str(self.queue.get())
                print("from queue", command)
                if 'break' in command.lower():
                    break
            with self.lock:
                try:
                    lv_voltage=self.LV.GetActualVoltage()
                    lv_current=self.LV.GetActualCurrent()
                    print(type(lv_voltage))
                    print(float(str(lv_voltage)))
                    graphyte.send("QCBox.LVVoltage",float(lv_voltage))
                    graphyte.send("QCBox.LVCurrent",float(lv_current))
                except Exception as e:
                    print(str(e))
                    print("low voltage not responding")

            with self.lock:
                try:
                    data=self.interlock.get_data()
                    data=data.split(",")
                    metrics=["hybrid NTC 1",
                             "hybrid NTC 2",
                             "hybrid NTC 3",
                             "hybrid NTC 4",
                             "hybrid NTC 5",
                             "hybrid NTC 6",
                             "hybrid NTC 7",
                             "hybrid NTC 8",
                             "user NTC 1",
                             "user NTC 2",
                             "SHT_temp",
                             "SHT_humidity"]
                    
                    for point,metric in zip(data,metrics):
                        point=int(point)
                        name="interlock."+metric
                        graphyte.send(name,point)
                except AttributeError:
                    print("interlock not responding")
                    
            for label,sensor in SensorDict.items():
                try:
                    sensor_items=sensor.get_data().items()
                except:
                    print("problem with "+label)
                    continue
                for name,data in sensor_items:
                    try:
                        grafana_name='QCBox.'+str(label)
                        grafana_name+="."+name
                        print(grafana_name,data)
                        graphyte.send(grafana_name,float(data))
                    except (sht_sensor.sensor.ShtCRCCheckError,sht_sensor.sensor.ShtCommFailure):
                        print("SHT sensor in config file but not connected properly")
                    except AttributeError:
                        print("encountered a problem with "+label)
            time.sleep(0.5)


class ThermalCycle(threading.Thread):
    def __init__(self,threadID,comm_queue,DAQ_client,sensor,Chiller,message,total_cycles=4,target_temperature=15,room_temperature=20):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.chiller = Chiller
        self.queue=comm_queue
        self.lock = threading.Lock()
        self.DAQ_client=DAQ_client
        self.target_temperature=target_temperature
        self.room_temperature=room_temperature
        self.total_cycles=total_cycles
        self.sensor=sensor
        self.message=message
        self.chiller.SetTemperature(int(self.room_temperature)+1)
        print("set chiller to ", int(self.room_temperature)+1)

    def run(self):
        print ("Starting " + str(self.threadID))
        self.loop()
        
        
    def loop(self):
        print("STARTED THERMAL CYCLE THREAD","UPPER TEMP= ",self.room_temperature,"LOWER BOUND= ",self.target_temperature)
        Cycle='High'
        number=0
        finished_cycle=False
        msg=''
        temperature_deque=deque()
        while number<self.total_cycles:
            if number==0:
                msg=do_trims(self.message)
            else:
                msg=self.message
            time.sleep(0.5)
            temp=self.sensor.get_data()['channel_4']
            graphyte.send("QCBox.ThermalCycle",temp)
            temperature_deque.append(temp)
            if len(temperature_deque)>4:
                temperature_deque.popleft()

            print("thermal cycle temp",temp,finished_cycle)
            if (not self.queue.empty()):
                command=str(self.queue.get())
                print("from queue", command)
                if 'break' in command.lower():
                    print("ending thermal cycle")
                    break
                if 'change_temp' in command.lower():
                    if Cycle=='High' and temp >= self.room_temperature:
                        self.chiller.SetTemperature(int(self.target_temperature)-1)
                        print("chiller set to ",int(self.target_temperature)-1)
                        Cycle='Low'
                        
                        finished_cycle=False
                    if Cycle=='Low' and temp <=self.target_temperature:
                        self.chiller.SetTemperature(int(self.room_temperature)+1)
                        print("chiller set to",self.room_temperature+1)
                        Cycle='High'
                        number+=1
                        finished_cycle=False
                      
            else:
               
                if Cycle=='High' and temp >= self.room_temperature and not finished_cycle and check_stability(temperature_deque):
                    print("sending server to do itsdaq test at high")
                    print(temp)
                    finished_cycle=True
                    print("tests", msg)
                    DAQ_client.SendServerMessage(msg)
                    
                elif Cycle=='Low' and temp <= self.target_temperature and not finished_cycle and check_stability(temperature_deque):
                    finished_cycle=True
                    print("sending server to do itsdaq test at low")
                    print(temp)
                    print("tests", msg)
                    DAQ_client.SendServerMessage(msg)
                    
        
